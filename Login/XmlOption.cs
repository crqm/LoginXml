﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.IO;

namespace Login
{
    class XmlOption
    {
        static string path = @"../../users.xml";
        static public bool writexml(Users u)
        {
            bool resent = false;
            try
            {
                if (File.Exists(path))
                {
                    XElement xml = XElement.Load(path);
                    xml.Add(new XElement("user",
                        new XElement("username",u.UserName),
                        new XElement("password",u.Password)));
                    xml.Save(path);
                }
                else
                {
                    XElement xml = new XElement("users",
                        new XElement("user",
                            new XElement("username", u.UserName),
                            new XElement("password", u.Password)));
                    xml.Save(path);
                }
                resent = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
            return resent;
        }
        static public bool readxml(Users u)
        {
            bool resent = false;
            try
            {
                XElement xml = XElement.Load(path);
                foreach (var v in xml.Elements())
                {
                    resent = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
            return resent;
        }
        static public bool readxml(string userName)
        {
            bool result = false;
            try
            {
                XElement xml = XElement.Load(path);
                foreach (var v in xml.Elements())
                {
                    if (v.Element("username").Value == userName)
                    {
                        result = true;
                        return result;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
            return result;
        }

    }
}
