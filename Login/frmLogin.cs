﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Login
{
    public partial class frmLogin : Form
    {
        public frmLogin()
        {
            InitializeComponent();
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            Users u = new Users();
            u.UserName = txtUser.Text.Trim();
            u.Password = txtPassword.Text.Trim();
            //if(txtUser.Text.Trim()=="admin"&&txtPassword.Text.Trim()=="123")
            //if(FileOption.readFile(u))
            if(XmlOption.readxml(u))
            {
                this.Hide();
                frmMain f = new frmMain();
                f.ShowDialog();
            }
            else
            {
                MessageBox.Show("登录失败！", "很遗憾", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            frmReg ff = new frmReg();
            ff.ShowDialog();
        }
    }
}
