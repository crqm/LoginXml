﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Login
{
    public partial class frmReg : Form
    {
        public frmReg()
        {
            InitializeComponent();
        }

        private void btnReg_Click(object sender, EventArgs e)
        {
            Users u=new Users();
            u.UserName = txtUser.Text.Trim();
            u.Password = txtPassword.Text.Trim();
            if (txtUser.Text.Trim() == "" || txtPassword.Text.Trim() == "" || txtPassword1.Text.Trim() == "")
            {
                MessageBox.Show("所填注册信息不全，请继续填写！");
                return;
            }
            if (txtPassword.Text.Trim() != txtPassword1.Text.Trim())
            {
                MessageBox.Show("两次输入的密码不一致！");
                return;
            }
            //if(FileOption.readFile(u.UserName))
            if(XmlOption.readxml(u.UserName))
            {
                MessageBox.Show("此用户已注册！");
                return;
            }
            //if (FileOption.writeFile(u))
            if(XmlOption.writexml(u))
                MessageBox.Show("注册成功！");
            else
                MessageBox.Show("注册失败！");
        }
    }
}
